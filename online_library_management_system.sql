-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2024 at 03:39 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_library_management_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nation` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `name`, `nation`, `date_of_birth`, `created_at`, `updated_at`) VALUES
('671d7239-d684-4fe3-802f-5f464a494bd8', 'Albert Indrew', 'German', '1987-06-09', '2024-06-10 16:42:28', '2024-06-10 16:42:28.483646'),
('abda3958-5074-4bf1-a639-2c488ef2c8e0', 'Saana', 'Khmer', '2024-06-09', '2024-06-10 14:28:56', '2024-06-10 14:28:56.025122');

-- --------------------------------------------------------

--
-- Table structure for table `author_books_book`
--

CREATE TABLE `author_books_book` (
  `authorId` varchar(36) NOT NULL,
  `bookId` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` varchar(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `isbn` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `cover_image_url` varchar(255) NOT NULL,
  `published_date` datetime DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `libraryId` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `isbn`, `language`, `cover_image_url`, `published_date`, `description`, `created_at`, `updated_at`, `libraryId`) VALUES
('31b5035a-fc60-46cc-9fb6-f6e84e95cc2a', 'Learning Javascript V1', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 15:25:57', '2024-06-10 15:25:57.915260', '32e1cb80-943a-448e-b35a-8e3fa40f98d9'),
('43632bff-bed7-48fe-9fb2-04e1fdf27070', 'World War 2', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 17:44:36', '2024-06-10 17:44:36.944175', '32e1cb80-943a-448e-b35a-8e3fa40f98d9'),
('51135c5c-9b3e-4ea5-a237-b2023c94b781', 'Learning Javascript V2', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 17:38:12', '2024-06-10 17:38:12.782247', '32e1cb80-943a-448e-b35a-8e3fa40f98d9'),
('6fd62091-8b50-4cc9-bd0f-f99a40b241cf', 'Learn Nodejs in 10 minutes', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 19:19:18', '2024-06-10 19:19:18.672169', '4681169f-7fd8-4433-90e8-a5c62e446493'),
('aed569aa-3acc-4c89-b6ea-e478c77486db', 'World War 1', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 19:15:45', '2024-06-10 19:15:45.891453', NULL),
('c01e12fc-31f7-4fd5-9a66-cb85a180869f', 'Learn Nodejs in 10 minutes', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 19:25:52', '2024-06-10 19:25:52.565566', '4681169f-7fd8-4433-90e8-a5c62e446493'),
('fcf5fd39-264e-4421-8e2e-46fb99e46111', 'World War 1', 'INV-00912', 'English', 'https://123.png', '2019-01-09 07:00:00', 'Hello world', '2024-06-10 19:16:41', '2024-06-10 19:16:41.911160', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`) VALUES
('200f4f3e-473b-40cc-9a96-149878a99c92', 'History', '2024-06-10 14:40:58', '2024-06-10 14:40:58.063561'),
('e7d61c11-ed25-4c27-ae92-c82147aa23ce', 'Sience', '2024-06-10 15:52:58', '2024-06-10 15:52:58.384889'),
('eadbf132-976c-4150-9770-0aaa31358d56', 'Coding', '2024-06-10 18:13:08', '2024-06-10 18:13:08.958217');

-- --------------------------------------------------------

--
-- Table structure for table `category_books_book`
--

CREATE TABLE `category_books_book` (
  `categoryId` varchar(36) NOT NULL,
  `bookId` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category_books_book`
--

INSERT INTO `category_books_book` (`categoryId`, `bookId`) VALUES
('200f4f3e-473b-40cc-9a96-149878a99c92', '31b5035a-fc60-46cc-9fb6-f6e84e95cc2a'),
('200f4f3e-473b-40cc-9a96-149878a99c92', '43632bff-bed7-48fe-9fb2-04e1fdf27070'),
('200f4f3e-473b-40cc-9a96-149878a99c92', '51135c5c-9b3e-4ea5-a237-b2023c94b781'),
('200f4f3e-473b-40cc-9a96-149878a99c92', '6fd62091-8b50-4cc9-bd0f-f99a40b241cf'),
('200f4f3e-473b-40cc-9a96-149878a99c92', 'aed569aa-3acc-4c89-b6ea-e478c77486db'),
('200f4f3e-473b-40cc-9a96-149878a99c92', 'c01e12fc-31f7-4fd5-9a66-cb85a180869f'),
('200f4f3e-473b-40cc-9a96-149878a99c92', 'fcf5fd39-264e-4421-8e2e-46fb99e46111');

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE `library` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`id`, `name`, `location`, `created_at`, `updated_at`) VALUES
('32e1cb80-943a-448e-b35a-8e3fa40f98d9', 'Lego Library', 'St 271, Terk Laork3, Phnom Penh', '2024-06-10 15:24:57', '2024-06-10 15:24:57.422058'),
('3b3afce8-beec-4e3e-845d-4eb5c31a63bf', 'Sokha Library', 'St 271, Terk Laork3, Phnom Penh', '2024-06-10 16:45:01', '2024-06-10 19:07:45.000000'),
('4681169f-7fd8-4433-90e8-a5c62e446493', 'Phnom Penh Library', 'St 271, Terk Laork3, Phnom Penh', '2024-06-10 19:02:49', '2024-06-10 19:02:49.055403');

-- --------------------------------------------------------

--
-- Table structure for table `library_books_book`
--

CREATE TABLE `library_books_book` (
  `libraryId` varchar(36) NOT NULL,
  `bookId` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `currency` enum('USD','RIEL') NOT NULL DEFAULT 'USD',
  `duration` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `name`, `price`, `currency`, `duration`, `created_at`, `updated_at`) VALUES
('007f6bc8-2c06-4af1-8c0a-726c0ca83ae4', 'VVIP', 5.99, 'USD', 3, '2024-06-10 18:03:31', '2024-06-10 18:03:31.707022'),
('13b2b458-ef70-4d4d-b717-0525caf93df2', 'Pro', 1.99, 'USD', 3, '2024-06-10 11:24:05', '2024-06-10 11:24:05.128463'),
('7b3bd2b0-0832-4804-8ead-e4f95eb91f7e', 'Basic', 1.99, 'USD', 4, '2024-06-10 16:40:54', '2024-06-10 18:01:49.000000'),
('97e489ce-abdb-40ab-8fb0-9e386e703eb1', 'Premuim', 4.99, 'USD', 3, '2024-06-10 14:31:18', '2024-06-10 14:31:18.292920');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(36) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('ADMIN','USER','GUEST','Library Owner') NOT NULL DEFAULT 'USER',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `planId` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `role`, `created_at`, `updated_at`, `planId`) VALUES
('199f0033-7b89-45e4-9620-9b0e07ff8cd4', 'Lolita', 'lolita@gmail.com', '$2a$10$W7lqs8GzHOruI33ZDTsEkeVRaq8AsTfO3TvyKjbCMGbDz5uznWzZG', 'Library Owner', '2024-06-10 18:59:11', '2024-06-10 19:02:49.000000', '7b3bd2b0-0832-4804-8ead-e4f95eb91f7e'),
('54de7fd6-0aa6-4eb6-9c43-aeffc02a7d12', 'koko', 'koko@gmail.com', '$2a$10$Ty9Tj5eGfoZheibOC3VrUO7GnkoSlI9VQ9Do4vSdsL3voE99VwXsu', 'Library Owner', '2024-06-10 11:11:29', '2024-06-10 16:45:01.000000', NULL),
('6964119e-46a0-4170-b160-61ca97e361b9', 'Kesor', 'kesor@gmail.com', '$2a$10$rOLPD36DVT0N5KptoX.Z6OoTgM/pMpOnYJZnooMeG/YyEkl/J8XQC', 'USER', '2024-06-10 19:31:45', '2024-06-10 20:02:00.000000', '13b2b458-ef70-4d4d-b717-0525caf93df2'),
('e30ae690-2237-4e40-b44b-8174104dd415', 'Somphors', 'somphors@gmail.com', '$2a$10$.JDaSKiN8Bef.MS/mEfkh.l.oYw230EdVJsrb/hApBYyxgk2BbYym', 'ADMIN', '2024-06-10 11:10:39', '2024-06-10 20:01:29.000000', '13b2b458-ef70-4d4d-b717-0525caf93df2'),
('f7c59f30-80d9-41c9-88f9-67ce4e8df243', 'lala', 'lala@gmail.com', '$2a$10$yP9omipizjHpeboolR261eMO2G9.KRh/n0WtRbPPR/lClU7cZe3eK', 'GUEST', '2024-06-10 11:24:39', '2024-06-10 11:24:39.575857', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author_books_book`
--
ALTER TABLE `author_books_book`
  ADD PRIMARY KEY (`authorId`,`bookId`),
  ADD KEY `IDX_e9ac29df6d093aa0b8079f1d15` (`authorId`),
  ADD KEY `IDX_34342925729041ac5301b289a9` (`bookId`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_da052b08a5b50d4601bb0f15ac0` (`libraryId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_23c05c292c439d77b0de816b50` (`name`);

--
-- Indexes for table `category_books_book`
--
ALTER TABLE `category_books_book`
  ADD PRIMARY KEY (`categoryId`,`bookId`),
  ADD KEY `IDX_c91e15c7afdefb644e19eefc06` (`categoryId`),
  ADD KEY `IDX_b2ac429a6afcd2495c59b27e84` (`bookId`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library_books_book`
--
ALTER TABLE `library_books_book`
  ADD PRIMARY KEY (`libraryId`,`bookId`),
  ADD KEY `IDX_8911b7dcd3a4715b738f13726a` (`libraryId`),
  ADD KEY `IDX_86038be4092a9aa040029a1c89` (`bookId`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_8aa73af67fa634d33de9bf874a` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`),
  ADD KEY `FK_40f6ab3925c167d26e52db93cf0` (`planId`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `author_books_book`
--
ALTER TABLE `author_books_book`
  ADD CONSTRAINT `FK_34342925729041ac5301b289a9a` FOREIGN KEY (`bookId`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_e9ac29df6d093aa0b8079f1d151` FOREIGN KEY (`authorId`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `FK_da052b08a5b50d4601bb0f15ac0` FOREIGN KEY (`libraryId`) REFERENCES `library` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `category_books_book`
--
ALTER TABLE `category_books_book`
  ADD CONSTRAINT `FK_b2ac429a6afcd2495c59b27e845` FOREIGN KEY (`bookId`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_c91e15c7afdefb644e19eefc066` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `library_books_book`
--
ALTER TABLE `library_books_book`
  ADD CONSTRAINT `FK_86038be4092a9aa040029a1c896` FOREIGN KEY (`bookId`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_8911b7dcd3a4715b738f13726ac` FOREIGN KEY (`libraryId`) REFERENCES `library` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_40f6ab3925c167d26e52db93cf0` FOREIGN KEY (`planId`) REFERENCES `plan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
