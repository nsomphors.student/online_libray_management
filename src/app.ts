// import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import cors from 'cors';
import { Tags } from 'opentracing';
import { ENV } from './constant';
import { buildSchema } from 'type-graphql';
import { JSONStringParser } from './utils';
import Resolvers from './graphql/resolvers';
import { createConnection } from 'typeorm';

const startServer = async () => {
  const app = express();
  app.use(express.json());
  app.use(cors());

  await createConnection()
    .then(async () => {
      console.log('connection to database success');
    })
    .catch((error) => console.log(error));

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: Resolvers as any,
      validate: true,
    }),
    context: ({ req }) => {
      return {
        // serviceCode: req.headers['service-code'],
        userContext: JSONStringParser(req.headers['user-context']),
      };
    },
    formatError: (error) => {
      if (global.span) {
        global.span.setTag(Tags.ERROR, true);
        global.span.log({
          code: error.extensions.code,
          message: error.message,
        });
      }
      return {
        code: error.extensions.code,
        message: error.message,
      };
    },

    introspection: true || ENV.ENABLE_INTROSPECTION,
  });

  await apolloServer.start();

  // apollo server middleware
  apolloServer.applyMiddleware({ app, path: '/graphql' });

  const port = ENV.APP_PORT;
  const httpServer = app.listen(port, () => {
    console.log(`GraphQL Server Listening on Port ${port}`);
  });

  return httpServer;
};

startServer();
