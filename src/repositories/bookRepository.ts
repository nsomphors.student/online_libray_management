import { getRepository } from 'typeorm';
import { Book } from '../entities';
import { AnyObject } from '../types';
import { PagerInput } from '../graphql/typeDefs';
import { BookFilter } from '../entities/book';

export interface BookRepository {
  countDocument(query: unknown): Promise<number>;
  findOne(query: AnyObject): Promise<Book>;
  find(pager: PagerInput, filter: BookFilter): Promise<Book[]>;
  findWhere(query: AnyObject): Promise<Book[]>;
  create(data: AnyObject): Promise<Book>;
  delete(bookId: string): Promise<boolean>;
  remove(books: Book[]): Promise<Book[]>;
  createOrUpdate(query: AnyObject, update: AnyObject): Promise<Book>;
}

export class BookRepositoryImpl implements BookRepository {
  private modelBook = getRepository(Book);

  async countDocument(query: unknown): Promise<number> {
    return await this.modelBook.countBy(query);
  }

  async findOne(query: AnyObject): Promise<Book | null> {
    const book = await this.modelBook.findOne({
      where: query,
      relations: ['authors', 'categories', 'library'],
    });
    return book;
  }

  async findWhere(query: AnyObject): Promise<Book[]> {
    const books = await this.modelBook.find({
      where: query,
    });
    return books;
  }

  async find(pager: PagerInput, filter: BookFilter): Promise<Book[] | null> {
    const skip = (pager.page - 1) * pager.limit;
    const queryBuilder = this.modelBook
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'authors')
      .leftJoinAndSelect('book.categories', 'categories')
      .leftJoinAndSelect('book.library', 'library')
      .skip(skip)
      .take(pager.limit);

    // Add filter if name is provided
    if (filter) {
      queryBuilder.where('book.title LIKE :title', {
        title: `%${filter.title}%`,
      });
    }
    const books = await queryBuilder.getMany();
    return books;
  }

  async create(data: AnyObject): Promise<Book> {
    return await this.modelBook.save(data);
  }

  async delete(bookId: string): Promise<boolean> {
    const data = await this.modelBook.delete({ id: bookId });
    return data.affected == 1 ? true : false;
  }

  async remove(books: Book[]): Promise<Book[]> {
    const data = await this.modelBook.remove(books);
    return data;
  }

  async createOrUpdate(query: AnyObject, update: AnyObject): Promise<Book> {
    return await this.modelBook.save(query, update);
  }
}
