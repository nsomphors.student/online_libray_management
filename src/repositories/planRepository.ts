import { getRepository } from 'typeorm';
import { Plan } from '../entities';
import { AnyObject } from '../types';
import { PlanInput } from '../entities/plan';
import { PagerInput } from '../graphql/typeDefs';

export interface PlanRepository {
  countDocument(query: unknown): Promise<number>;
  findOne(query: AnyObject): Promise<Plan>;
  find(pager: PagerInput): Promise<Plan[]>;
  create(data: PlanInput): Promise<Plan>;
  delete(planId: string): Promise<boolean>;
  update(plan: Plan, update: AnyObject): Promise<Plan>;
}

export class PlanRepositoryImpl implements PlanRepository {
  private modelPlan = getRepository(Plan);

  async countDocument(query: unknown): Promise<number> {
    return await this.modelPlan.countBy(query);
  }

  async findOne(query: AnyObject): Promise<Plan | null> {
    const plan = await this.modelPlan.findOne({
      where: query,
      relations: ['users'],
    });
    return plan;
  }

  async find(pager: PagerInput): Promise<Plan[] | null> {
    const skip = (pager.page - 1) * pager.limit;
    const plans = await this.modelPlan
      .createQueryBuilder('plan')
      .leftJoinAndSelect('plan.users', 'users')
      .skip(skip)
      .take(pager.limit)
      .getMany();
    return plans;
  }

  async create(data: PlanInput): Promise<Plan> {
    return await this.modelPlan.save(data);
  }

  async delete(planId: string): Promise<boolean> {
    const plan = await this.modelPlan.delete({ id: planId });

    return plan.affected == 1 ? true : false;
  }

  async update(plan: Plan, update: AnyObject): Promise<Plan> {
    Object.assign(plan, update);
    return await this.modelPlan.save(plan);
  }
}
