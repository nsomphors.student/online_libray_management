import { getRepository } from 'typeorm';
import { Category } from '../entities';
import { AnyObject } from '../types';
import { PagerInput } from '../graphql/typeDefs';

export interface CategoryRepository {
  countDocument(query: unknown): Promise<number>;
  findOne(query: AnyObject): Promise<Category>;
  findByIds(categoryIds: string[]): Promise<Category[]>;
  find(pager: PagerInput): Promise<Category[]>;
  create(name: string): Promise<Category>;
  delete(categoryId: string): Promise<boolean>;
  createOrUpdate(query: AnyObject, update: AnyObject): Promise<Category>;
}

export class CategoryRepositoryImpl implements CategoryRepository {
  private modelCategory = getRepository(Category);

  async countDocument(query: unknown): Promise<number> {
    return await this.modelCategory.countBy(query);
  }

  async findOne(query: AnyObject): Promise<Category | null> {
    const category = await this.modelCategory.findOne({
      where: query,
      relations: ['books'],
    });
    return category;
  }

  async findByIds(categoryIds: string[]): Promise<Category[]> {
    const categories = await this.modelCategory.findByIds(categoryIds);
    return categories;
  }

  async find(pager: PagerInput): Promise<Category[] | null> {
    const skip = (pager.page - 1) * pager.limit;
    const categories = await this.modelCategory
      .createQueryBuilder('category')
      .leftJoinAndSelect('category.books', 'books')
      .skip(skip)
      .take(pager.limit)
      .getMany();
    return categories;
  }

  async create(name: string): Promise<Category> {
    const category = await this.modelCategory.save({ name });
    return category;
  }

  async delete(categoryId: string): Promise<boolean> {
    const category = await this.modelCategory.delete({ id: categoryId });
    return category.affected == 1 ? true : false;
  }

  async createOrUpdate(query: AnyObject, update: AnyObject): Promise<Category> {
    return await this.modelCategory.save(query, update);
  }
}
