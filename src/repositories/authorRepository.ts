import { Author } from '../entities';
import { getRepository } from 'typeorm';
import { AuthorInput } from '../entities/author';
import { PagerInput } from '../graphql/typeDefs';

export interface AuthorRepository {
  countDocument(query: unknown): Promise<number>;
  findOne(authorId: string): Promise<Author>;
  findByIds(authorIds: string[]): Promise<Author[]>;
  find(pager: PagerInput): Promise<Author[]>;
  create(author: AuthorInput): Promise<Author>;
  delete(authorId: string): Promise<boolean>;
}

export class AuthorRepositoryImpl implements AuthorRepository {
  private authorModel = getRepository(Author);

  async countDocument(query: unknown): Promise<number> {
    return await this.authorModel.countBy(query);
  }

  async findByIds(authorIds: string[]): Promise<Author[]> {
    const authors = await this.authorModel.findByIds(authorIds);
    return authors;
  }

  async findOne(authorId: string): Promise<Author> {
    const author = await this.authorModel.findOne({
      where: { id: authorId },
      relations: ['books'],
    });
    return author ? author : null;
  }

  async find(pager: PagerInput): Promise<Author[]> {
    const skip = (pager.page - 1) * pager.limit;
    const authors = await this.authorModel
      .createQueryBuilder('author')
      .leftJoinAndSelect('author.books', 'books')
      .skip(skip)
      .take(pager.limit)
      .getMany();
    return authors;
  }

  async create(author: AuthorInput): Promise<Author> {
    const authorData = {
      name: author.name,
      nation: author.nation,
      date_of_birth: new Date(author.dateOfBirth),
    };
    const data = await this.authorModel.save(authorData);
    return data;
  }

  async delete(authorId: string): Promise<boolean> {
    const data = await this.authorModel.delete({ id: authorId });
    return data.affected == 1 ? true : false;
  }
}
