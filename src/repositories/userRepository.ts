import { getRepository } from 'typeorm';
import { User } from '../entities';
import { AnyObject } from '../types';
import { UserInput } from '../entities/user';
import { PagerInput } from '../graphql/typeDefs';

export interface UserRepository {
  countDocument(query: unknown): Promise<number>;
  findOne(query: AnyObject): Promise<User>;
  find(pager: PagerInput): Promise<User[]>;
  create(data: UserInput): Promise<User>;
  delete(userId: string): Promise<boolean>;
  update(user: User, update: AnyObject): Promise<User>;
}

export class UserRepositoryImpl implements UserRepository {
  private modelUser = getRepository(User);

  async countDocument(query: unknown): Promise<number> {
    return await this.modelUser.countBy(query);
  }

  async findOne(query: AnyObject): Promise<User | null> {
    const user = await this.modelUser.findOne({
      where: query,
      relations: ['plan'],
    });
    return user;
  }

  async find(pager: PagerInput): Promise<User[] | null> {
    const skip = (pager.page - 1) * pager.limit;
    const users = await this.modelUser
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.plan', 'plan')
      .skip(skip)
      .take(pager.limit)
      .getMany();

    return users;
  }

  async create(data: UserInput): Promise<User> {
    return await this.modelUser.save(data);
  }

  async delete(userId: string): Promise<boolean> {
    const user = await this.modelUser.delete({ id: userId });

    return user.affected == 1 ? true : false;
  }

  async update(user: User, update: AnyObject): Promise<User> {
    Object.assign(user, update);

    return await this.modelUser.save(user);
  }
}
