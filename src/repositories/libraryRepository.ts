import { getRepository } from 'typeorm';
import { Library } from '../entities';
import { AnyObject } from '../types';
import { LibraryInput } from '../entities/library';
import { PagerInput } from '../graphql/typeDefs';

export interface LibraryRepository {
  countDocument(query: unknown): Promise<number>;
  findOne(query: AnyObject): Promise<Library>;
  find(pager: PagerInput): Promise<Library[]>;
  create(data: LibraryInput): Promise<Library>;
  delete(libraryId: string): Promise<boolean>;
  update(library: Library, update: AnyObject): Promise<Library>;
}

export class LibraryRepositoryImpl implements LibraryRepository {
  private modelLibrary = getRepository(Library);

  async countDocument(query: unknown): Promise<number> {
    return await this.modelLibrary.countBy(query);
  }

  async findOne(query: AnyObject): Promise<Library | null> {
    const library = await this.modelLibrary.findOne({
      where: query,
      relations: ['books'],
    });
    return library;
  }

  async find(pager: PagerInput): Promise<Library[] | null> {
    const skip = (pager.page - 1) * pager.limit;
    const libraries = await this.modelLibrary
      .createQueryBuilder('library')
      .leftJoinAndSelect('library.books', 'books')
      .skip(skip)
      .take(pager.limit)
      .getMany();
    return libraries;
  }

  async create(data: LibraryInput): Promise<Library> {
    return await this.modelLibrary.save(data);
  }

  async delete(libraryId: string): Promise<boolean> {
    const library = await this.modelLibrary.delete({ id: libraryId });

    return library.affected == 1 ? true : false;
  }

  async update(library: Library, update: AnyObject): Promise<Library> {
    Object.assign(library, update);
    return await this.modelLibrary.save(library);
  }
}
