import { ObjectType, Field as GqlField, ID, InputType } from 'type-graphql';
import {
  Column as DBField,
  Entity,
  JoinTable,
  ManyToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Book } from './book';
import PaginatedResponse from '../graphql/typeDefs/paginationTypeDef';
import { User } from './user';

@ObjectType()
@Entity()
export class Library {
  @GqlField(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @GqlField()
  @DBField()
  name: string;

  @GqlField()
  @DBField()
  location: string;

  @GqlField({ nullable: true })
  @OneToOne(() => User, (user) => user.id)
  owner: User;

  @GqlField(() => [Book], { nullable: true })
  @ManyToMany(() => Book, (book) => book.library, { cascade: false })
  @JoinTable()
  books: Book[];

  @GqlField(() => Date, { name: 'createdAt', nullable: true })
  @DBField({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @GqlField(() => Date, { name: 'updatedAt', nullable: true })
  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}

@InputType()
export class LibraryInput {
  @GqlField()
  ownerId: string;

  @GqlField()
  name: string;

  @GqlField({ nullable: true })
  location: string;
}

@InputType()
export class LibraryUpdateInput extends LibraryInput {
  @GqlField()
  libraryId: string;
}

@ObjectType()
export class LibraryUpdateResponse {
  @GqlField()
  message: string;

  @GqlField(() => Library)
  library: Library;
}

@ObjectType()
export class ListLibraryResponse extends PaginatedResponse(Library) {}
