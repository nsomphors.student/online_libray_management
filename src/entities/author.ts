import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column as DBField,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { ObjectType, Field as GqlField, ID, InputType } from 'type-graphql';
import { Book } from './book';
import PaginatedResponse from '../graphql/typeDefs/paginationTypeDef';
import { v4 as uuidv4 } from 'uuid';

@Entity()
@ObjectType()
export class Author extends BaseEntity {
  @GqlField(() => ID)
  @PrimaryGeneratedColumn('uuid')
  // id: string;
  id = uuidv4();

  @GqlField()
  @DBField()
  name: string;

  @GqlField(() => String)
  @DBField({ nullable: true })
  nation: string;

  @GqlField(() => String, { name: 'dateOfBirth', nullable: true })
  @DBField({ type: 'date', nullable: true })
  date_of_birth: Date;

  @GqlField(() => [Book], { nullable: true })
  @ManyToMany(() => Book, (book) => book.authors, { cascade: false })
  @JoinTable()
  books: Book[];

  @GqlField(() => Date, { name: 'createdAt', nullable: true })
  @DBField({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @GqlField(() => Date, { name: 'updatedAt', nullable: true })
  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}

@InputType()
export class AuthorInput {
  @GqlField()
  name: string;

  @GqlField({ nullable: true })
  nation: string;

  @GqlField({ nullable: true })
  dateOfBirth: string; //2007-12-03
}

@ObjectType()
export class ListAuthorResponse extends PaginatedResponse(Author) {}
