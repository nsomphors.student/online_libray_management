import {
  ObjectType,
  Field as GqlField,
  ID,
  InputType,
  registerEnumType,
} from 'type-graphql';
import {
  Column as DBField,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user';
import { v4 as uuidv4 } from 'uuid';
import { CURRENCY } from '../types';
import PaginatedResponse from '../graphql/typeDefs/paginationTypeDef';

@ObjectType()
@Entity()
@Unique(['name'])
export class Plan {
  @GqlField(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id = uuidv4();

  @GqlField()
  @DBField()
  name: string;

  @GqlField()
  @DBField({ type: 'float' })
  price: number;

  @GqlField(() => CURRENCY)
  @DBField({ type: 'enum', enum: CURRENCY, default: CURRENCY.USD })
  currency: CURRENCY;

  @GqlField(() => String)
  @DBField()
  duration: number;

  @GqlField(() => [User], { nullable: true })
  @OneToMany(() => User, (user) => user.plan)
  users: User[];

  @GqlField(() => Date, { name: 'createdAt', nullable: true })
  @DBField({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @GqlField(() => Date, { name: 'updatedAt', nullable: true })
  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}

registerEnumType(CURRENCY, {
  name: 'Currency', // this one is mandatory
  description: 'The currency type of price', // this one is optional
});

@InputType()
export class PlanInput {
  @GqlField()
  name: string;

  @GqlField()
  price: number;

  @GqlField(() => CURRENCY)
  currency: CURRENCY;

  @GqlField()
  duration: number;
}

@ObjectType()
export class PlanUpdateResponse {
  @GqlField()
  message: string;

  @GqlField(() => Plan)
  plan: Plan;
}

@ObjectType()
export class ListPlanResponse extends PaginatedResponse(Plan) {}
