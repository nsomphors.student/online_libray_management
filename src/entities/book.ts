import { ObjectType, Field as GqlField, ID, InputType } from 'type-graphql';
import {
  Column as DBField,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Author } from './author';
import { Category } from './category';
import { Library } from './library';
import { v4 as uuidv4 } from 'uuid';
import PaginatedResponse from '../graphql/typeDefs/paginationTypeDef';

@ObjectType()
@Entity()
export class Book {
  @GqlField(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id = uuidv4();

  @GqlField()
  @DBField()
  title: string;

  @GqlField()
  @DBField()
  isbn: string;

  @GqlField()
  @DBField()
  language: string;

  @GqlField({ name: 'coverImageURL' })
  @DBField()
  cover_image_url: string;

  @GqlField({ name: 'publishedDate' })
  @DBField({ nullable: true })
  published_date: Date;

  @GqlField({ nullable: true })
  @DBField()
  description: string;

  @GqlField(() => [Author], { nullable: true })
  @ManyToMany(() => Author, (author) => author.books, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  authors: Author[];

  @GqlField(() => [Category], { nullable: true })
  @ManyToMany(() => Category, (category) => category.books, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  categories: Category[];

  @GqlField(() => Library, { nullable: true })
  @ManyToOne(() => Library, (library) => library.books, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  library: Library;

  @GqlField(() => Date, { name: 'createdAt', nullable: true })
  @DBField({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @GqlField(() => Date, { name: 'updatedAt', nullable: true })
  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}

@InputType()
export class BookInput {
  @GqlField()
  title: string;

  @GqlField({ nullable: true })
  isbn: string;

  @GqlField({ nullable: true })
  description: string;

  @GqlField({ nullable: true })
  coverImageURL: string;

  @GqlField()
  language: string; //2007-12-03

  @GqlField({ nullable: true })
  publishedDate: string; //2007-12-03

  @GqlField(() => [String])
  authors: string[];

  @GqlField(() => [String])
  categories: string[];

  @GqlField()
  library: string;
}

@InputType()
export class BookFilter {
  @GqlField()
  title: string;
}

@ObjectType()
export class ListBookResponse extends PaginatedResponse(Book) {}
