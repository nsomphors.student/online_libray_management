import {
  Entity,
  PrimaryGeneratedColumn,
  Column as DBField,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  Unique,
} from 'typeorm';
import { ObjectType, Field as GqlField, ID } from 'type-graphql';
import { Book } from './book';
import PaginatedResponse from '../graphql/typeDefs/paginationTypeDef';

@ObjectType()
@Entity()
@Unique(['name'])
export class Category {
  @GqlField(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @GqlField()
  @DBField({ type: String })
  name: string;

  @GqlField(() => [Book], { nullable: true })
  @ManyToMany(() => Book, (book) => book.categories, {
    cascade: false,
    nullable: true,
  })
  @JoinTable()
  books: Book[];

  @GqlField(() => Date, { name: 'createdAt', nullable: true })
  @DBField({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @GqlField(() => Date, { name: 'updatedAt', nullable: true })
  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}

@ObjectType()
export class ListCategoryResponse extends PaginatedResponse(Category) {}
