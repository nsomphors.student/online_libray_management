import {
  Entity,
  PrimaryGeneratedColumn,
  Column as DBField,
  UpdateDateColumn,
  ManyToOne,
  Unique,
} from 'typeorm';
import {
  ObjectType,
  Field as GqlField,
  ID,
  registerEnumType,
  InputType,
} from 'type-graphql';
import { USER_ROLE } from '../constant';
import { IsEmail } from 'class-validator';
import { Plan } from './plan';
import PaginatedResponse from '../graphql/typeDefs/paginationTypeDef';

@ObjectType()
@Entity()
@Unique(['email'])
export class User {
  @GqlField(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @GqlField()
  @DBField()
  username: string;

  @GqlField()
  @DBField()
  @IsEmail()
  email: string;

  @DBField()
  password: string;

  @GqlField(() => USER_ROLE)
  @DBField({
    type: 'enum',
    enum: USER_ROLE,
    default: USER_ROLE.USER,
  })
  role: USER_ROLE;

  @GqlField({ nullable: true })
  token: string;

  @GqlField(() => Plan, { nullable: true })
  @ManyToOne(() => Plan, (plan) => plan.users)
  plan: Plan;

  @GqlField(() => Date, { name: 'createdAt', nullable: true })
  @DBField({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @GqlField(() => Date, { name: 'updatedAt', nullable: true })
  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}

registerEnumType(USER_ROLE, {
  name: 'UserRole', // this one is mandatory
  description: 'The basic roles of users', // this one is optional
});

@InputType()
export class UserInput {
  @GqlField()
  username: string;

  @GqlField()
  email: string;

  @GqlField()
  password: string;

  @GqlField(() => USER_ROLE)
  role: USER_ROLE;
}

@InputType()
export class UserSignInInput {
  @GqlField()
  username: string;

  @GqlField()
  password: string;
}

@InputType()
export class UpdateUserInput {
  @GqlField()
  username: string;

  @GqlField()
  password: string;

  @GqlField()
  planName: string;
}

@ObjectType()
export class UserUpdateResponse {
  @GqlField()
  message: string;

  @GqlField(() => User)
  user: User;
}

@ObjectType()
export class ListUserResponse extends PaginatedResponse(User) {}
