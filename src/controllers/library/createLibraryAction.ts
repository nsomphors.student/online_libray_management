import { USER_ROLE } from '../../constant';
import { Library } from '../../entities';
import { LibraryInput } from '../../entities/library';
import { LibraryService, UserService } from '../../services';

const createLibraryAction = async (
  library: LibraryInput
): Promise<Library | null> => {
  const { name, ownerId } = library;
  if (!name) throw new Error('Library Name is required');
  if (!ownerId) throw new Error('Library owner is required');

  const libraryService = new LibraryService();
  const userService = new UserService();
  const user = await userService.getUserDetail({
    id: ownerId,
    role: USER_ROLE.OWNER,
  });
  if (!user) throw new Error('Library owner not exist');
  library['owner'] = user;
  const libraryData = await libraryService.createLibrary(library);

  return libraryData;
};

export default createLibraryAction;
