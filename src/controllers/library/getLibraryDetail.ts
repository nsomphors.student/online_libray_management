import { Library } from '../../entities';
// import { AuthorRepositoryImpl } from '../../repositories';
import { LibraryService } from '../../services';

// const authorRepositoryImpl = new AuthorRepositoryImpl();

const getLibraryDetailAction = async (
  libraryId: string
): Promise<Library | null> => {
  if (!libraryId) throw new Error('Library Id is required');
  const libraryService = new LibraryService();

  const data = await libraryService.getLibraryDetail({ id: libraryId });

  if (!data) throw new Error(`Library Id ${libraryId} is not found`);

  return data;
};

export default getLibraryDetailAction;
