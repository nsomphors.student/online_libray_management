import { CommonResponse } from '../../graphql/typeDefs';
import { BookService, LibraryService } from '../../services';

const deleteLibraryAction = async (
  libraryId: string
): Promise<CommonResponse> => {
  if (!libraryId) throw new Error('Library Id is required');

  const libraryService = new LibraryService();
  const bookService = new BookService();

  const getLibrary = await libraryService.getLibraryDetail({
    id: libraryId,
  });

  if (!getLibrary) throw new Error('Library not found');

  const library = await libraryService.deleteLibrary(libraryId);
  if (!library) throw new Error('Cannot delete Library');

  const books = await bookService.filterBook({ library: getLibrary });

  await bookService.removeBooks(books);

  return {
    message: 'Delete Library successfully',
    success: true,
  };
};

export default deleteLibraryAction;
