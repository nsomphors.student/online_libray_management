import { ListLibraryResponse } from '../../entities/library';
import { PagerInput } from '../../graphql/typeDefs';
import { LibraryService } from '../../services';

const listLibraryAction = async (
  pager: PagerInput
): Promise<ListLibraryResponse> => {
  const libraryService = new LibraryService();

  const data = await libraryService.getLibraryList(pager);
  return data;
};

export default listLibraryAction;
