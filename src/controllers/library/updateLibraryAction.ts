import { LibraryService } from '../../services';
import {
  LibraryUpdateInput,
  LibraryUpdateResponse,
} from '../../entities/library';

const updateLibraryAction = async (
  library: LibraryUpdateInput
): Promise<LibraryUpdateResponse> => {
  const { libraryId } = library;
  if (!libraryId) {
    throw new Error('Library Id is required');
  }

  const libraryService = new LibraryService();

  const existingLibrary = await libraryService.getLibraryDetail({
    id: libraryId,
  });
  if (!existingLibrary) {
    throw new Error('Library not exists');
  }

  const updateLibrary = await libraryService.updateLibrary(existingLibrary, {
    ...library,
  });

  return { message: 'Library updated successfully', library: updateLibrary };
};

export default updateLibraryAction;
