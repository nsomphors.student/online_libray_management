import { PlanInput, PlanUpdateResponse } from '../../entities/plan';
import { PlanService } from '../../services';

const updatePlanAction = async ({
  plan,
  planId,
}: {
  plan: PlanInput;
  planId: string;
}): Promise<PlanUpdateResponse> => {
  if (!planId) throw new Error('Plan Id is required');
  const planService = new PlanService();

  const data = await planService.getPlanDetail({ id: planId });
  if (!data) throw new Error(`Plan Id ${planId} is not found`);

  const planUpdate = await planService.updatePlan(data, plan);

  planUpdate['users'] = data.users;

  return {
    message: 'Plan updated successfully',
    plan: planUpdate,
  };
};

export default updatePlanAction;
