import { CommonResponse } from '../../graphql/typeDefs';
import { PlanService } from '../../services';

const deletePlanAction = async (planId: string): Promise<CommonResponse> => {
  if (!planId) throw new Error('Plan Id is required');

  const planService = new PlanService();

  const getPlan = await planService.getPlanDetail({
    id: planId,
  });

  if (!getPlan) throw new Error('Plan not found');

  const plan = await planService.deletePlan(planId);

  if (!plan) throw new Error('Cannot delete Plan');

  return {
    message: 'Delete Plan successfully',
    success: true,
  };
};

export default deletePlanAction;
