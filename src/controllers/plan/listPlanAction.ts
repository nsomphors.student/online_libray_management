import { ListPlanResponse } from '../../entities/plan';
import { PagerInput } from '../../graphql/typeDefs';
import { PlanService } from '../../services';

const listPlanAction = async (pager: PagerInput): Promise<ListPlanResponse> => {
  const planService = new PlanService();

  const data = await planService.getPlanList(pager);
  return data;
};

export default listPlanAction;
