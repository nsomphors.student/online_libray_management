import { Plan } from '../../entities';
// import { AuthorRepositoryImpl } from '../../repositories';
import { PlanService } from '../../services';

// const authorRepositoryImpl = new AuthorRepositoryImpl();

const getPlanDetailAction = async (planId: string): Promise<Plan | null> => {
  if (!planId) throw new Error('Library Id is required');
  const planService = new PlanService();

  const data = await planService.getPlanDetail({ id: planId });

  if (!data) throw new Error(`Plan Id ${planId} is not found`);

  return data;
};

export default getPlanDetailAction;
