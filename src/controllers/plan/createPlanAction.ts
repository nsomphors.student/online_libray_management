import { Plan } from '../../entities';
import { PlanInput } from '../../entities/plan';
import { PlanService } from '../../services';

const createPlanAction = async (plan: PlanInput): Promise<Plan | null> => {
  const { name } = plan;
  if (!name) throw new Error('Plan Name is required');

  const planService = new PlanService();

  //   if (category) throw new Error('Category name already exist');

  return await planService.createPlan(plan);
};

export default createPlanAction;
