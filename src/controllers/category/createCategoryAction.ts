import { Category } from '../../entities';
import { CategoryService } from '../../services';

const createCategoryAction = async (name: string): Promise<Category | null> => {
  if (!name) throw new Error('Category Name is required');

  const categoryService = new CategoryService();

  const category = await categoryService.createCategory(name);

  return category;
};

export default createCategoryAction;
