import { ListCategoryResponse } from '../../entities/category';
import { PagerInput } from '../../graphql/typeDefs';
// import { AuthorRepositoryImpl } from '../../repositories';
import { CategoryService } from '../../services';

// const authorRepositoryImpl = new AuthorRepositoryImpl();

const listCategoryAction = async (
  pager: PagerInput
): Promise<ListCategoryResponse> => {
  const categoryService = new CategoryService();

  const data = await categoryService.getCategoryList(pager);
  return data;
};

export default listCategoryAction;
