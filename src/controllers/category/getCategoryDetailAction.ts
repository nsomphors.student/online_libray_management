import { Category } from '../../entities';
import { CategoryService } from '../../services';

const getCategoryDetailAction = async (
  categoryId: string
): Promise<Category | null> => {
  if (!categoryId) throw new Error('Category Id is required');
  const categoryService = new CategoryService();

  const data = await categoryService.getCategoryDetail({ id: categoryId });

  if (!data) throw new Error(`Book Id ${categoryId} is not found`);

  return data;
};

export default getCategoryDetailAction;
