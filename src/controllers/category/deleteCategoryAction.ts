import { CommonResponse } from '../../graphql/typeDefs';
import { CategoryService } from '../../services';

const deleteCategoryAction = async (
  categoryId: string
): Promise<CommonResponse> => {
  if (!categoryId) throw new Error('category Id is required');

  const categoryService = new CategoryService();

  const getCategory = await categoryService.getCategoryDetail({
    id: categoryId,
  });

  if (!getCategory) throw new Error('Category not found');

  const category = await categoryService.deleteCategory(categoryId);

  if (!category) throw new Error('Cannot delete category');

  return {
    message: 'Delete Category successfully',
    success: true,
  };
};

export default deleteCategoryAction;
