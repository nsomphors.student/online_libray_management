import { Book } from '../../entities';
import { BookInput } from '../../entities/book';
import {
  AuthorService,
  BookService,
  CategoryService,
  LibraryService,
} from '../../services';
import { isValidDateFormat } from '../../utils';

const createBookAction = async (bookInput: BookInput): Promise<Book | null> => {
  const {
    title,
    publishedDate,
    isbn,
    description,
    coverImageURL,
    language,
    authors,
    categories,
    library,
  } = bookInput;
  if (!title) throw new Error('Book title is required');
  if (!authors || authors.length == 0)
    throw new Error('Book title is required');
  if (!categories || categories.length == 0)
    throw new Error('Book category is required');
  if (!library) throw new Error('Book library is required');

  if (publishedDate) {
    if (!isValidDateFormat(publishedDate))
      throw new Error('DateOfBirth is invalid format');
  }
  const book = {
    title,
    published_date: new Date(publishedDate),
    isbn,
    description,
    language,
    cover_image_url: coverImageURL,
  };

  const bookService = new BookService();
  const authorService = new AuthorService();
  const categoryService = new CategoryService();
  const libraryService = new LibraryService();

  const authorsData = await authorService.getAuthorByIds(authors);
  const categoriesData = await categoryService.getCategoryByIds(categories);
  const libraryData = await libraryService.getLibraryDetail({ id: library });

  book['authors'] = authorsData;
  book['categories'] = categoriesData;
  book['library'] = libraryData;

  const data = await bookService.createBook(book);

  return data;
};

export default createBookAction;
