import { BookFilter, ListBookResponse } from '../../entities/book';
import { PagerInput } from '../../graphql/typeDefs';
// import { AuthorRepositoryImpl } from '../../repositories';
import { BookService } from '../../services';

// const authorRepositoryImpl = new AuthorRepositoryImpl();

const listBookAction = async ({
  pager,
  filter,
}: {
  pager: PagerInput;
  filter: BookFilter;
}): Promise<ListBookResponse> => {
  const bookService = new BookService();

  const data = await bookService.getBookList(pager, filter);
  return data;
};

export default listBookAction;
