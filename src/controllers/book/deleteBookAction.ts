import { CommonResponse } from '../../graphql/typeDefs';
import { BookService } from '../../services';

const deleteBookByIdAction = async (
  bookId: string
): Promise<CommonResponse> => {
  if (!bookId) throw new Error('Author Id is required');

  const bookService = new BookService();

  const data = await bookService.deleteBookById(bookId);

  if (!data) throw new Error('Cannot delete author');

  return {
    message: 'Delete Book successfully',
    success: true,
  };
};

export default deleteBookByIdAction;
