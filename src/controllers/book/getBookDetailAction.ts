import { Book } from '../../entities';
import { BookService } from '../../services';

const getBookDetailAction = async (bookId: string): Promise<Book | null> => {
  if (!bookId) throw new Error('Book Id is required');
  const bookService = new BookService();

  const data = await bookService.getBookDetail(bookId);

  if (!data) throw new Error(`Book Id ${bookId} is not found`);

  return data;
};

export default getBookDetailAction;
