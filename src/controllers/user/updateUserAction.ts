import { PlanService, UserService } from '../../services';
import bcrypt from 'bcryptjs';
import { UpdateUserInput, UserUpdateResponse } from '../../entities/user';

const updateUserAction = async (
  user: UpdateUserInput
): Promise<UserUpdateResponse> => {
  const { username, password, planName } = user;

  if (!username || !password) {
    throw new Error('Username and password are required');
  }

  const userService = new UserService();
  const planService = new PlanService();

  const existingUser = await userService.getUserDetail({ username });
  if (!existingUser) {
    throw new Error('User not exists');
  }
  const isPasswordValid = await bcrypt.compare(password, existingUser.password);
  if (!isPasswordValid) {
    throw new Error('Invalid password');
  }

  const plan = await planService.getPlanDetail({ name: planName });
  const newUser = existingUser;
  newUser['plan'] = plan;

  const updateUser = await userService.updateUser(existingUser, newUser);

  return { message: 'User updated successfully', user: updateUser };
};

export default updateUserAction;
