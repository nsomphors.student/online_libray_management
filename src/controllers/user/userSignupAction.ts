import { User } from '../../entities';
import { PlanService, UserService } from '../../services';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { UserInput } from '../../entities/user';
import { ENV } from '../../constant';

const userSignupAction = async (user: UserInput): Promise<User | null> => {
  const { username, password } = user;

  if (!username || !password) {
    throw new Error('Username and password are required');
  }

  const userService = new UserService();
  const planService = new PlanService();

  const existingUser = await userService.getUserDetail({ username });
  if (existingUser) {
    throw new Error('User already exists');
  }
  const plan = await planService.getPlanDetail({ name: 'Basic' });

  user['plan'] = plan;
  const hashedPassword = await bcrypt.hash(password, 10);
  user.password = hashedPassword;

  const token = jwt.sign({ username }, ENV.JWT_SECRET, { expiresIn: '48h' });

  //   if (category) throw new Error('Category name already exist');
  const userData = await userService.createUser(user);

  userData['plan'] = plan;
  return { ...userData, token };
};

export default userSignupAction;
