import { UserService } from '../../services';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { ENV } from '../../constant';
import { UserLoginResponse } from '../../graphql/typeDefs';
import { UserSignInInput } from '../../entities/user';

const userSignInAction = async (
  user: UserSignInInput
): Promise<UserLoginResponse> => {
  const { username, password } = user;

  if (!username || !password) {
    throw new Error('Username and password are required');
  }

  const userService = new UserService();

  const existingUser = await userService.getUserDetail({ username });
  if (!existingUser) {
    throw new Error('User not exists');
  }
  const isPasswordValid = await bcrypt.compare(password, existingUser.password);
  if (!isPasswordValid) {
    throw new Error('Invalid password');
  }

  const token = jwt.sign({ username }, ENV.JWT_SECRET, { expiresIn: '48h' });

  return { statusCode: 200, message: 'User login successfully', token };
};

export default userSignInAction;
