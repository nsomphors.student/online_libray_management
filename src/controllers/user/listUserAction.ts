import { ListUserResponse } from '../../entities/user';
import { PagerInput } from '../../graphql/typeDefs';
import { UserService } from '../../services';

const listUserAction = async (pager: PagerInput): Promise<ListUserResponse> => {
  const userService = new UserService();

  const data = await userService.getUserList(pager);
  return data;
};

export default listUserAction;
