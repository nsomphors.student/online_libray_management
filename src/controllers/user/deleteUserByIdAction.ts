import { CommonResponse } from '../../graphql/typeDefs';
import { UserService } from '../../services';

const deleteUserAction = async (userId: string): Promise<CommonResponse> => {
  if (!userId) throw new Error('User Id is required');

  const userService = new UserService();

  const getUser = await userService.getUserDetail({
    id: userId,
  });

  if (!getUser) throw new Error('User not found');

  const user = await userService.deleteUser(userId);

  if (!user) throw new Error('Cannot delete User');

  return {
    message: 'Delete Plan successfully',
    success: true,
  };
};

export default deleteUserAction;
