import { User } from '../../entities';
// import { AuthorRepositoryImpl } from '../../repositories';
import { UserService } from '../../services';

// const authorRepositoryImpl = new AuthorRepositoryImpl();

const getUserDetailAction = async (userId: string): Promise<User | null> => {
  if (!userId) throw new Error('Library Id is required');
  const userService = new UserService();

  const data = await userService.getUserDetail({ id: userId });

  if (!data) throw new Error(`User Id ${userId} is not found`);

  return data;
};

export default getUserDetailAction;
