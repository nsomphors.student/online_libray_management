import { CommonResponse } from '../../graphql/typeDefs';
import { AuthorService } from '../../services';

const deleteAuthorByIdAction = async (
  authorId: string
): Promise<CommonResponse> => {
  if (!authorId) throw new Error('Author Id is required');

  const authorService = new AuthorService();

  const data = await authorService.deleteAuthorById(authorId);

  if (!data) throw new Error('Cannot delete author');

  return {
    message: 'Delete Author successfully',
    success: true,
  };
};

export default deleteAuthorByIdAction;
