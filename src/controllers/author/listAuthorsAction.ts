import { ListAuthorResponse } from '../../entities/author';
import { PagerInput } from '../../graphql/typeDefs';
import { AuthorService } from '../../services';

const listAuthorAction = async (
  pager: PagerInput
): Promise<ListAuthorResponse> => {
  const authorService = new AuthorService();

  const data = await authorService.getAuthorList(pager);
  return data;
};

export default listAuthorAction;
