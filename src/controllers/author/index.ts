import listAuthorAction from './listAuthorsAction';
import getAuthorDetailAction from './getAuthorDetailAction';
import createAuthorAction from './createAuthorAction';
import deleteAuthorByIdAction from './deleteAuthorByIdAction';

export {
  listAuthorAction,
  getAuthorDetailAction,
  createAuthorAction,
  deleteAuthorByIdAction,
};
