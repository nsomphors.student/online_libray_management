import { Author } from '../../entities';
import { AuthorService } from '../../services';

const getAuthorDetailAction = async (
  authorId: string
): Promise<Author | null> => {
  if (!authorId) throw new Error('Author Id is required');
  const authorService = new AuthorService();

  const data = await authorService.getAuthorDetail(authorId);

  if (!data) throw new Error(`Author Id ${authorId} is not found`);

  return data;
};

export default getAuthorDetailAction;
