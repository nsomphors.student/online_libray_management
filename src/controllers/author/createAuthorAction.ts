import { Author } from '../../entities';
import { AuthorInput } from '../../entities/author';
import { AuthorService } from '../../services';
import { isValidDateFormat } from '../../utils';

const createAuthorAction = async (
  authorInput: AuthorInput
): Promise<Author | null> => {
  const { name, dateOfBirth } = authorInput;
  if (!name) throw new Error('Author name is required');

  if (dateOfBirth) {
    if (!isValidDateFormat(dateOfBirth))
      throw new Error('DateOfBirth is invalid format');
  }

  const authorService = new AuthorService();

  const data = await authorService.createAuthor(authorInput);

  return data;
};

export default createAuthorAction;
