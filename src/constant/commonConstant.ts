const SORT_BY = {
  ASC: 'asc',
  DESC: 'desc',
};

const ENVIRONMENT = {
  TEST: 'test',
  CI_TEST: 'ci_test',
};

const ERROR_MESSAGES = {
  INTERNAL_SERVER_ERROR: 'Internal server error',
};

const STATUS = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
};

enum USER_ROLE {
  ADMIN = 'ADMIN',
  USER = 'USER',
  GUEST = 'GUEST',
  OWNER = 'Library Owner',
}

export { SORT_BY, ENVIRONMENT, ERROR_MESSAGES, STATUS, USER_ROLE };
