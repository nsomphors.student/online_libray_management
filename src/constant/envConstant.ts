import dotEnv from 'dotenv';

if (process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'ci_test') {
  dotEnv.config({ path: './src/test/.test.env' });
}

const {
  APP_PORT,
  NODE_ENV,
  IMG,
  TAG,
  ROW_LIMIT,
  ENABLE_INTROSPECTION,
  RETRY_INTERVAL,
  INPUT_ROW_LIMIT,
  MAX_LIMIT,
  DB_NAME,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  JWT_SECRET,
} = process.env;
const ENV = {
  APP_PORT: APP_PORT || 3000,
  NODE_ENV,
  IMG,
  TAG,
  ROW_LIMIT: Number(ROW_LIMIT) || 20,
  ENABLE_INTROSPECTION: ENABLE_INTROSPECTION === 'true',
  RETRY_INTERVAL: RETRY_INTERVAL || 3000,
  INPUT_ROW_LIMIT: INPUT_ROW_LIMIT || 100,
  MAX_LIMIT: Number(MAX_LIMIT) || 100,
  DB_NAME,
  DB_USER,
  DB_PASSWORD: DB_PASSWORD || '',
  DB_HOST,
  DB_PORT,
  JWT_SECRET,
};

export default ENV;
