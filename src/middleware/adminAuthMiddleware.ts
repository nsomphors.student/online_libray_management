import { MiddlewareFn } from 'type-graphql';
import { AnyObject } from '../types';
import { UserService } from '../services';
import bcrypt from 'bcryptjs';
import { USER_ROLE } from '../constant';

const adminAuthMiddleware: MiddlewareFn<AnyObject> = async (
  { context },
  next
) => {
  try {
    const userService = new UserService();
    const user = await userService.getUserDetail({
      username: context.userContext?.user?.username,
    });

    if (!user) {
      throw new Error('Invalid User');
    }

    const isPasswordValid = await bcrypt.compare(
      context.userContext?.user?.password,
      user.password
    );
    if (!isPasswordValid) {
      throw new Error('Invalid password');
    }
    if (user.role !== USER_ROLE.ADMIN) {
      throw new Error('Unauthorized. This endpoint only access by Admin');
    }
    return next();
  } catch (error) {
    return error;
  }
};

export default adminAuthMiddleware;
