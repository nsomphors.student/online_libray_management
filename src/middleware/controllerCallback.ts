const controllerCallback = async <T, R>(
  actionHandler: (arg: T, ctx?: unknown) => R,
  args: T,
  ctx?: unknown
): Promise<R | Error> => {
  try {
    return await actionHandler(args, ctx);
  } catch (error) {
    return error;
  }
};

export default controllerCallback;
