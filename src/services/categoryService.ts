/* istanbul ignore file */
import { ENV } from '../constant';
import { Category } from '../entities';
import { ListCategoryResponse } from '../entities/category';
import { PagerInput } from '../graphql/typeDefs';
import { CategoryRepository, CategoryRepositoryImpl } from '../repositories';
import { AnyObject } from '../types';
import { paginations } from '../utils/paginationUtil';

export class CategoryService {
  private categoryRepository: CategoryRepository = new CategoryRepositoryImpl();

  // constructor(authorRepository: BookRepository) {
  //   this.categoryRepository = authorRepository;
  // }

  async getCategoryDetail(filter: AnyObject): Promise<Category> {
    return await this.categoryRepository.findOne(filter);
  }

  async getCategoryByIds(categoryIds: string[]): Promise<Category[]> {
    return await this.categoryRepository.findByIds(categoryIds);
  }

  async getCategoryList(pager: PagerInput): Promise<ListCategoryResponse> {
    if (!pager?.page) pager = { ...pager, page: 1 };
    if (!pager?.limit || pager.limit > ENV.MAX_LIMIT) {
      pager.limit = ENV.ROW_LIMIT;
    }
    const authors = await this.categoryRepository.find(pager);
    const totalAuthors = await this.categoryRepository.countDocument({});

    const categoryListWithPagination: ListCategoryResponse = paginations(
      authors,
      totalAuthors,
      pager?.page,
      pager?.limit
    );

    return categoryListWithPagination;
  }

  async createCategory(name: string): Promise<Category> {
    return await this.categoryRepository.create(name);
  }

  async deleteCategory(categoryId: string): Promise<boolean> {
    return await this.categoryRepository.delete(categoryId);
  }
}
