/* istanbul ignore file */
import { ENV } from '../constant';
import { Plan } from '../entities';
import { ListPlanResponse, PlanInput } from '../entities/plan';
import { PagerInput } from '../graphql/typeDefs';
import { PlanRepository, PlanRepositoryImpl } from '../repositories';
import { AnyObject } from '../types';
import { paginations } from '../utils/paginationUtil';

export class PlanService {
  private planRepository: PlanRepository = new PlanRepositoryImpl();

  // constructor(authorRepository: PlanRepository) {
  //   this.planRepository = authorRepository;
  // }

  async getPlanDetail(query: AnyObject): Promise<Plan> {
    return await this.planRepository.findOne(query);
  }

  async getPlanList(pager: PagerInput): Promise<ListPlanResponse> {
    if (!pager?.page) pager = { ...pager, page: 1 };
    if (!pager?.limit || pager.limit > ENV.MAX_LIMIT) {
      pager.limit = ENV.ROW_LIMIT;
    }
    const authors = await this.planRepository.find(pager);
    const totalAuthors = await this.planRepository.countDocument({});

    const planListWithPagination: ListPlanResponse = paginations(
      authors,
      totalAuthors,
      pager?.page,
      pager?.limit
    );

    return planListWithPagination;
  }

  async createPlan(data: PlanInput): Promise<Plan> {
    return await this.planRepository.create(data);
  }

  async deletePlan(planId: string): Promise<boolean> {
    return await this.planRepository.delete(planId);
  }

  async updatePlan(plan: Plan, updatePlan: AnyObject): Promise<Plan> {
    return await this.planRepository.update(plan, updatePlan);
  }
}
