/* istanbul ignore file */
import { ENV } from '../constant';
import { Book } from '../entities';
import { BookFilter, ListBookResponse } from '../entities/book';
import { PagerInput } from '../graphql/typeDefs';
import { BookRepository, BookRepositoryImpl } from '../repositories';
import { AnyObject } from '../types';
import { paginations } from '../utils/paginationUtil';

export class BookService {
  private bookRepository: BookRepository = new BookRepositoryImpl();

  // constructor(authorRepository: BookRepository) {
  //   this.bookRepository = authorRepository;
  // }

  async getBookDetail(bookId: string): Promise<Book> {
    return await this.bookRepository.findOne({ id: bookId });
  }

  async filterBook(query: AnyObject): Promise<Book[]> {
    return await this.bookRepository.findWhere(query);
  }

  async getBookList(
    pager: PagerInput,
    filter: BookFilter
  ): Promise<ListBookResponse> {
    if (!pager?.page) pager = { ...pager, page: 1 };
    if (!pager?.limit || pager.limit > ENV.MAX_LIMIT) {
      pager.limit = ENV.ROW_LIMIT;
    }
    const authors = await this.bookRepository.find(pager, filter);
    const totalAuthors = await this.bookRepository.countDocument(filter);

    const bookListWithPagination: ListBookResponse = paginations(
      authors,
      totalAuthors,
      pager?.page,
      pager?.limit
    );

    return bookListWithPagination;
  }

  async createBook(data: AnyObject): Promise<Book> {
    return await this.bookRepository.create(data);
  }

  async deleteBookById(bookId: string): Promise<boolean> {
    return await this.bookRepository.delete(bookId);
  }

  async removeBooks(books: Book[]): Promise<Book[]> {
    return await this.bookRepository.remove(books);
  }
}
