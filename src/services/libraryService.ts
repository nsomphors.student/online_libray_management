/* istanbul ignore file */
import { ENV } from '../constant';
import { Library } from '../entities';
import { LibraryInput, ListLibraryResponse } from '../entities/library';
import { PagerInput } from '../graphql/typeDefs';
import { LibraryRepository, LibraryRepositoryImpl } from '../repositories';
import { AnyObject } from '../types';
import { paginations } from '../utils/paginationUtil';

export class LibraryService {
  private libraryRepository: LibraryRepository = new LibraryRepositoryImpl();

  // constructor(authorRepository: LibraryRepository) {
  //   this.libraryRepository = authorRepository;
  // }

  async getLibraryDetail(query: AnyObject): Promise<Library> {
    return await this.libraryRepository.findOne(query);
  }

  async getLibraryList(pager: PagerInput): Promise<ListLibraryResponse> {
    if (!pager?.page) pager = { ...pager, page: 1 };
    if (!pager?.limit || pager.limit > ENV.MAX_LIMIT) {
      pager.limit = ENV.ROW_LIMIT;
    }
    const authors = await this.libraryRepository.find(pager);
    const totalAuthors = await this.libraryRepository.countDocument({});

    const libraryListWithPagination: ListLibraryResponse = paginations(
      authors,
      totalAuthors,
      pager?.page,
      pager?.limit
    );

    return libraryListWithPagination;
  }

  async createLibrary(data: LibraryInput): Promise<Library> {
    return await this.libraryRepository.create(data);
  }

  async deleteLibrary(libraryId: string): Promise<boolean> {
    return await this.libraryRepository.delete(libraryId);
  }

  async updateLibrary(
    library: Library,
    libraryUpdate: AnyObject
  ): Promise<Library> {
    return await this.libraryRepository.update(library, libraryUpdate);
  }
}
