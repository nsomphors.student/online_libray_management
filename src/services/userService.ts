/* istanbul ignore file */
import { ENV } from '../constant';
import { User } from '../entities';
import { ListUserResponse, UserInput } from '../entities/user';
import { PagerInput } from '../graphql/typeDefs';
import { UserRepository, UserRepositoryImpl } from '../repositories';
import { AnyObject } from '../types';
import { paginations } from '../utils/paginationUtil';

export class UserService {
  private userRepository: UserRepository = new UserRepositoryImpl();

  // constructor(authorRepository: UserRepository) {
  //   this.userRepository = authorRepository;
  // }

  async getUserDetail(query: AnyObject): Promise<User> {
    return await this.userRepository.findOne(query);
  }

  async getUserList(pager: PagerInput): Promise<ListUserResponse> {
    if (!pager?.page) pager = { ...pager, page: 1 };
    if (!pager?.limit || pager.limit > ENV.MAX_LIMIT) {
      pager.limit = ENV.ROW_LIMIT;
    }
    const authors = await this.userRepository.find(pager);
    const totalAuthors = await this.userRepository.countDocument({});

    const userListWithPagination: ListUserResponse = paginations(
      authors,
      totalAuthors,
      pager?.page,
      pager?.limit
    );

    return userListWithPagination;
  }

  async createUser(data: UserInput): Promise<User> {
    return await this.userRepository.create(data);
  }

  async deleteUser(userId: string): Promise<boolean> {
    return await this.userRepository.delete(userId);
  }

  async updateUser(user: User, userUpdate: User): Promise<User> {
    return await this.userRepository.update(user, userUpdate);
  }
}
