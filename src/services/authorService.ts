/* istanbul ignore file */
import { ENV } from '../constant';
import { Author } from '../entities';
import { AuthorInput, ListAuthorResponse } from '../entities/author';
import { PagerInput } from '../graphql/typeDefs';
import { AuthorRepository, AuthorRepositoryImpl } from '../repositories';
import { paginations } from '../utils/paginationUtil';

export class AuthorService {
  private authorRepository: AuthorRepository = new AuthorRepositoryImpl();

  // constructor(authorRepository: AuthorRepository) {
  //   this.authorRepository = authorRepository;
  // }

  async getAuthorByIds(authorIds: string[]): Promise<Author[]> {
    return await this.authorRepository.findByIds(authorIds);
  }

  async getAuthorDetail(authorId: string): Promise<Author | null> {
    return await this.authorRepository.findOne(authorId);
  }

  async getAuthorList(pager: PagerInput): Promise<ListAuthorResponse> {
    if (!pager?.page) pager = { ...pager, page: 1 };
    if (!pager?.limit || pager.limit > ENV.MAX_LIMIT) {
      pager.limit = ENV.ROW_LIMIT;
    }
    const authors = await this.authorRepository.find(pager);
    const totalAuthors = await this.authorRepository.countDocument({});

    const authorListWithPagination: ListAuthorResponse = paginations(
      authors,
      totalAuthors,
      pager?.page,
      pager?.limit
    );

    return authorListWithPagination;
  }

  async createAuthor(data: AuthorInput): Promise<Author> {
    return await this.authorRepository.create(data);
  }

  async deleteAuthorById(authorId: string): Promise<boolean> {
    return await this.authorRepository.delete(authorId);
  }
}
