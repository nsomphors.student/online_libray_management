import {
  Int,
  Field as GqlField,
  ObjectType as GqlType,
  InputType as GqlInputType,
  registerEnumType,
} from 'type-graphql';

@GqlInputType()
export class PagerInput {
  @GqlField(() => Int)
  page: number;

  @GqlField(() => Int)
  limit: number;
}
export enum SortBy {
  ASC = 'asc',
  DESC = 'desc',
}

registerEnumType(SortBy, {
  name: 'SortBy',
  description: 'The query sort prototype',
});

@GqlType()
export class CommonResponse {
  @GqlField(() => String)
  message: string;

  @GqlField(() => Boolean)
  success: boolean;
}

@GqlType()
export class listSwarmIpResponse {
  @GqlField(() => [String])
  ipList: string[];
}

@GqlInputType()
export class AuthorFilter {
  @GqlField(() => String)
  name: string;
}

@GqlType()
export class UserLoginResponse {
  @GqlField()
  statusCode: number;

  @GqlField()
  message: string;

  @GqlField()
  token: string;
}

// @ObjectType()
// export class ApiResponse<T> {
//   @GqlField()
//   httpStatusCode: number;

//   @GqlField()
//   data: T | null;
// }

// @GqlType()
// export class ApiResponse {
//   @GqlField(() => Number)
//   httpStatusCode: number;

//   @GqlField()
//   data: T;
// }
