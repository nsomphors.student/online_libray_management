import { ClassType, Field, InputType, Int, ObjectType } from 'type-graphql';
import { ENV } from '../../constant';

@InputType()
export class FilterDateInput {
  @Field(() => String)
  startDate: string | number;

  @Field(() => String)
  endDate: string | number;
}

@InputType()
export class FilterSummaryInput {
  @Field()
  subscriptionId!: string;

  @Field()
  serviceCode!: string;

  @Field(() => FilterDateInput, { nullable: true })
  usageDate?: FilterDateInput;
}

@InputType()
export class Filter {
  @Field(() => Int, { nullable: true })
  mysabayUserID?: number;

  @Field(() => FilterDateInput, { nullable: true })
  dateIssued?: FilterDateInput;
}
@InputType()
export class PagerInput {
  @Field(() => Int, { defaultValue: 1 })
  page: number = 1;

  @Field(() => Int, { defaultValue: ENV.ROW_LIMIT })
  limit: number = ENV.ROW_LIMIT;
}

@ObjectType()
export class Pagination {
  @Field(() => Int)
  currentPage: number;

  @Field(() => Int)
  lastPage: number;

  @Field(() => Int)
  from: number;

  @Field(() => Int)
  to: number;

  @Field(() => Int)
  perPage: number;

  @Field(() => Int)
  total: number;
}

export default function PaginatedResponse<TItem extends object>(
  itemsField: ClassType<TItem>
) {
  @ObjectType()
  abstract class PaginatedResponseClass {
    @Field(() => [itemsField])
    documents: TItem[];

    @Field(() => Pagination, { nullable: true })
    pagination: Pagination;
  }
  return PaginatedResponseClass;
}
