import { Resolver, Query, Arg, Mutation } from 'type-graphql';
import { Book } from './../../entities';
import { controllerCallback } from '../../middleware';

import { CommonResponse, PagerInput } from '../typeDefs';
import {
  createBookAction,
  getBookDetailAction,
  listBookAction,
} from '../../controllers/book';
import { BookFilter, BookInput, ListBookResponse } from '../../entities/book';
import deleteBookByIdAction from '../../controllers/book/deleteBookAction';

@Resolver(Book)
export class BookResolver {
  @Query(() => ListBookResponse)
  async listBook(
    @Arg('pager', { nullable: true }) pager: PagerInput,
    @Arg('filter', { nullable: true }) filter: BookFilter
  ) {
    return await controllerCallback(listBookAction, { pager, filter });
  }

  @Query(() => Book || null)
  async getBookDetail(@Arg('bookId') bookId: string) {
    return await controllerCallback(getBookDetailAction, bookId);
  }

  @Mutation(() => Book)
  async createBook(
    // @Ctx() context: AnyObject,
    @Arg('bookInput') bookInput: BookInput
  ) {
    return await controllerCallback(createBookAction, bookInput);
  }

  @Mutation(() => CommonResponse)
  async deleteBookById(
    // @Ctx() context: AnyObject,
    @Arg('bookId') bookId: string
  ) {
    return await controllerCallback(deleteBookByIdAction, bookId);
  }
}
