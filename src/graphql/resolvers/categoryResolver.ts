import { Resolver, Query, Arg, Mutation } from 'type-graphql';
import { Category } from './../../entities';
import { controllerCallback } from '../../middleware';

import { CommonResponse, PagerInput } from '../typeDefs';
import {
  createCategoryAction,
  deleteCategoryAction,
  getCategoryDetailAction,
  listCategoryAction,
} from '../../controllers/category';
import { ListCategoryResponse } from '../../entities/category';

@Resolver()
export class CategoryResolver {
  @Query(() => ListCategoryResponse)
  async listCategory(@Arg('pager', { nullable: true }) pager: PagerInput) {
    return await controllerCallback(listCategoryAction, pager);
  }

  @Query(() => Category || null)
  async getCategoryDetail(@Arg('categoryId') categoryId: string) {
    return await controllerCallback(getCategoryDetailAction, categoryId);
  }

  @Mutation(() => Category)
  async createCategory(
    // @Ctx() context: AnyObject,
    @Arg('name') name: string
  ) {
    return await controllerCallback(createCategoryAction, name);
  }

  @Mutation(() => CommonResponse)
  async deleteCategoryById(
    // @Ctx() context: AnyObject,
    @Arg('authorId') authorId: string
  ) {
    return await controllerCallback(deleteCategoryAction, authorId);
  }
}
