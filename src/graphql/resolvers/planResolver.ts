import {
  Resolver,
  Query,
  Arg,
  Mutation,
  FieldResolver,
  Root,
  UseMiddleware,
  Ctx,
} from 'type-graphql';
import { Plan } from './../../entities';
import { controllerCallback } from '../../middleware';

import { CommonResponse, PagerInput } from '../typeDefs';
import {
  createPlanAction,
  deletePlanAction,
  getPlanDetailAction,
  listPlanAction,
  updatePlanAction,
} from '../../controllers/plan';
import {
  ListPlanResponse,
  PlanInput,
  PlanUpdateResponse,
} from '../../entities/plan';
import adminAuthMiddleware from '../../middleware/adminAuthMiddleware';
import { AnyObject } from '../../types';

@Resolver(Plan)
export class PlanResolver {
  @Query(() => ListPlanResponse)
  async listPlan(@Arg('pager', { nullable: true }) pager: PagerInput) {
    return await controllerCallback(listPlanAction, pager);
  }

  @Query(() => Plan || null)
  async getPlanDetail(@Arg('planId') planId: string) {
    return await controllerCallback(getPlanDetailAction, planId);
  }

  @Mutation(() => Plan)
  @UseMiddleware(adminAuthMiddleware)
  async createPlan(@Ctx() context: AnyObject, @Arg('plan') plan: PlanInput) {
    return await controllerCallback(createPlanAction, plan, context);
  }

  @Mutation(() => CommonResponse)
  @UseMiddleware(adminAuthMiddleware)
  async deletePlanById(
    @Ctx() context: AnyObject,
    @Arg('planId') planId: string
  ) {
    return await controllerCallback(deletePlanAction, planId, context);
  }

  @Mutation(() => PlanUpdateResponse)
  @UseMiddleware(adminAuthMiddleware)
  async updatePlan(
    @Ctx() context: AnyObject,
    @Arg('plan') plan: PlanInput,
    @Arg('planId') planId: string
  ) {
    return await controllerCallback(
      updatePlanAction,
      { plan, planId },
      context
    );
  }

  @FieldResolver(() => String)
  async duration(@Root() root: Plan): Promise<string> {
    return root.duration + ' Months';
  }
}
