import { BookResolver } from './bookResolver';
import { AuthorResolver } from './authorResolver';
import { UserResolver } from './userResolver';
import { CategoryResolver } from './categoryResolver';
import { LibraryResolver } from './libraryResolver';
import { PlanResolver } from './planResolver';

export default [
  BookResolver,
  AuthorResolver,
  UserResolver,
  CategoryResolver,
  LibraryResolver,
  PlanResolver,
];
