import {
  Resolver,
  Query,
  Arg,
  Mutation,
  UseMiddleware,
  Ctx,
} from 'type-graphql';
import { Library } from './../../entities';
import { controllerCallback } from '../../middleware';

import { CommonResponse, PagerInput } from '../typeDefs';
import {
  createLibraryAction,
  deleteLibraryAction,
  getLibraryDetailAction,
  listLibraryAction,
  updateLibraryAction,
} from '../../controllers/library';
import {
  LibraryInput,
  LibraryUpdateInput,
  LibraryUpdateResponse,
  ListLibraryResponse,
} from '../../entities/library';
import adminAuthMiddleware from '../../middleware/adminAuthMiddleware';
import { AnyObject } from '../../types';

@Resolver()
export class LibraryResolver {
  @Query(() => ListLibraryResponse)
  async listLibrary(@Arg('pager', { nullable: true }) pager: PagerInput) {
    return await controllerCallback(listLibraryAction, pager);
  }

  @Query(() => Library || null)
  async getLibraryDetail(@Arg('libraryId') libraryId: string) {
    return await controllerCallback(getLibraryDetailAction, libraryId);
  }

  @Mutation(() => Library)
  @UseMiddleware(adminAuthMiddleware)
  async createLibrary(
    @Ctx() context: AnyObject,
    @Arg('library') library: LibraryInput
  ) {
    return await controllerCallback(createLibraryAction, library, context);
  }

  @Mutation(() => CommonResponse)
  @UseMiddleware(adminAuthMiddleware)
  async deleteLibraryById(
    @Ctx() context: AnyObject,
    @Arg('libraryId') libraryId: string
  ) {
    return await controllerCallback(deleteLibraryAction, libraryId, context);
  }

  @Mutation(() => LibraryUpdateResponse)
  async updateLibrary(@Arg('library') library: LibraryUpdateInput) {
    return await controllerCallback(updateLibraryAction, library);
  }
}
