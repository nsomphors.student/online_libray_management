import {
  Resolver,
  Query,
  Arg,
  Mutation,
  UseMiddleware,
  Ctx,
} from 'type-graphql';
import { User } from './../../entities';
import { controllerCallback } from '../../middleware';

import {
  deleteUserAction,
  getUserDetailAction,
  listUserAction,
  updateUserAction,
  userSignInAction,
  userSignupAction,
} from '../../controllers/user';
import {
  ListUserResponse,
  UserInput,
  UserSignInInput,
  UserUpdateResponse,
} from '../../entities/user';
import { CommonResponse, PagerInput, UserLoginResponse } from '../typeDefs';
import adminAuthMiddleware from '../../middleware/adminAuthMiddleware';
import { AnyObject } from '../../types';

@Resolver(User)
export class UserResolver {
  @Query(() => ListUserResponse)
  @UseMiddleware(adminAuthMiddleware)
  async listUser(
    @Ctx() context: AnyObject,
    @Arg('pager', { nullable: true }) pager: PagerInput
  ) {
    return await controllerCallback(listUserAction, pager, context);
  }

  @Query(() => User || null)
  async getUserDetail(@Arg('userId') userId: string) {
    return await controllerCallback(getUserDetailAction, userId);
  }

  @Mutation(() => User)
  async userSignup(
    // @Ctx() context: AnyObject,
    @Arg('userInput') userInput: UserInput
  ) {
    return await controllerCallback(userSignupAction, userInput);
  }

  @Mutation(() => UserLoginResponse)
  async userSignIn(
    // @Ctx() context: AnyObject,
    @Arg('userInput')
    userInput: UserSignInInput
  ) {
    return await controllerCallback(userSignInAction, userInput);
  }

  @Mutation(() => CommonResponse)
  @UseMiddleware(adminAuthMiddleware)
  async deleteUserById(
    @Ctx() context: AnyObject,
    @Arg('userId') userId: string
  ) {
    return await controllerCallback(deleteUserAction, userId, context);
  }

  @Mutation(() => UserUpdateResponse)
  async updateUser(
    @Ctx() context: AnyObject,
    @Arg('planName') planName: string
  ) {
    const user = {
      username: context.userContext?.user?.username,
      password: context.userContext?.user?.password,
      planName,
    };
    return await controllerCallback(updateUserAction, user);
  }
}
