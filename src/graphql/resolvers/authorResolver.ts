import { Resolver, Query, Arg, Mutation } from 'type-graphql';
import { Author } from './../../entities';
import { controllerCallback } from '../../middleware';
import {
  createAuthorAction,
  deleteAuthorByIdAction,
  getAuthorDetailAction,
  listAuthorAction,
} from '../../controllers/author';
import { CommonResponse, PagerInput } from '../typeDefs';
import { AuthorInput, ListAuthorResponse } from '../../entities/author';

@Resolver(Author)
export class AuthorResolver {
  @Query(() => ListAuthorResponse)
  async listAuthor(@Arg('pager', { nullable: true }) pager: PagerInput) {
    return await controllerCallback(listAuthorAction, pager);
  }

  @Query(() => Author || null)
  async getAuthorDetail(@Arg('authorId') authorId: string) {
    return await controllerCallback(getAuthorDetailAction, authorId);
  }

  @Mutation(() => Author)
  async createAuthor(
    // @Ctx() context: AnyObject,
    @Arg('authorInout') authorInout: AuthorInput
  ) {
    return await controllerCallback(createAuthorAction, authorInout);
  }

  @Mutation(() => CommonResponse)
  async deleteAuthorById(
    // @Ctx() context: AnyObject,
    @Arg('authorId') authorId: string
  ) {
    return await controllerCallback(deleteAuthorByIdAction, authorId);
  }
}
