/* eslint-disable @typescript-eslint/no-explicit-any */
type SortOrderValue = 'asc' | 'desc';

type AnyObject = {
  [key: string]: any;
};

type ApiResponse<T> = {
  httpStatusCode: number;
  data: T | null;
};

enum CURRENCY {
  USD = 'USD',
  RIEL = 'RIEL',
}

export { AnyObject, SortOrderValue, ApiResponse, CURRENCY };
