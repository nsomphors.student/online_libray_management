export const isValidDateFormat = (dateString: string): boolean => {
  const dateFormatRegex = /^\d{4}-\d{2}-\d{2}$/;

  return dateFormatRegex.test(dateString);
};

export const JSONStringParser = (value: any): any => {
  try {
    return JSON.parse(value);
  } catch (error) {
    return value;
  }
};
