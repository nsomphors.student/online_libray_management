# api online_library_management

API for Online Library management

## Setup

1. Create a database mysql name `online_library_management_system`
2. Open the project, install modules run command `npm i`
3. To start the project run command `npm run dev`

## Lint check

Improve code with lint check first before submit any merge request

1. Install dependencies
2. Execute command<br /> `npm run lint`
3. Fix lint by updating code

## Unit test

This project using Jest for the test framework, to setup your local test environment follow the following steps:

1. Install dependencies
2. Inside `./src/test/` folder you should see `.test.env.example` file, copy it to `.test.env`
3. Inside project root directory, run command
   <br />`npm run test`

**Note:** The test case result must be passed with testing coverage mark of 80%  or higher
